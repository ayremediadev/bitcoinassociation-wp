<?php
/*
Plugin Name: Wapuu Dashboard Pet
Plugin URI: https://wapuudashboardpet.com
Description: Adds a digital pet to your site who lives off your updates.
Author: 34SP.com
Version: 1.1.3
Author URI: https://34sp.com
*/



class WapuuDashboardPet{

    
	/**
	* LOADS IN PRIMARY ACTIONS
	*
	* @since 1.0.0
	*
	* @return true
	*/
    function load()
    {

    //  $this->gotchi_update = wp_get_update_data();
      add_action('admin_init',array($this,'admin_init'));
      
      add_action( 'admin_menu', array($this, 'admin_menu') );
      
      add_action('WDPet_Weekly', array($this, 'weekly_email') );
            
            
             
    }


	/**
	* LOAD ADMIN INIT AND REGISTER OPTIONS
	*
	* @since 1.0.0
	*
	* @return true
	*/
    function admin_init()
    {
	    
	register_setting( 'WDPet-email', 'wapaemail' );
	
	 add_action( 'admin_footer_text', array($this, 'plugin_display') );


    }

	/**
	* ADD THE WapuuDashboardPet OPTIONS PAGE
	*
	* @since 1.0.0
	*
	* @return true
	*/
    function admin_menu() {
	    
	    
	add_options_page( 'Wapuu Dashboard Pet Options', 'Wapuu Dashboard Pet', 'manage_options', 'WDPet-options', array($this, 'options') );
    	
    }
    function options() {

		$gotchi_options_page = plugin_dir_path( __FILE__ );
		include_once($gotchi_options_page . 'templates/admin-page.php');
	}
	
	
	/**
	* CRON ACTIVATION
	*
	* @since 1.1.2
	*
	* @return true
	*/

	
	
	function cron_activation() {
		
if ( wp_next_scheduled ( 'WDPet_Daily' )) {
	wp_clear_scheduled_hook('WDPet_Daily');
		
	}

    if (! wp_next_scheduled ( 'WDPet_Weekly' )) {
	wp_schedule_event(time(), 'weekly', 'WDPet_Weekly');
    }	
    add_action( 'activated_plugin', array($this, 'activation_redirect') );
    }
    
    /**
	* CRON DEACTIVATION
	*
	* @since 1.0.0
	*
	* @return true
	*/
	function cron_deactivation() {
	wp_clear_scheduled_hook('WDPet_Weekly');
}
	
	/**
	* REDIRECT ON ACTIVATION
	*
	* @since 1.0.0
	*
	* @return true
	*/
	function activation_redirect( $plugin ) {
    if( $plugin == plugin_basename( __FILE__ ) ) {
        exit( wp_redirect( admin_url( 'options-general.php?page=WDPet-options' ) ) );
    }
}
	
	
	/**
	* RETRIEVE UPDATE DATA
	*
	* @since 1.0.0
	*
	* @return true
	*/
	function get_updates() {
		
		  $gotchi_updates_plugin_count = 0;
		  $gotchi_updates_core_count = 0;
		  $gotchi_updates_theme_count = 0;
  
		  $update_plugins = get_site_transient( 'update_plugins' );
    if ( ! empty( $update_plugins->response ) ){
    	$gotchi_updates_plugin_count = count( $update_plugins->response );
  }
 		$update_themes = get_site_transient( 'update_themes' );
  if ( ! empty( $update_themes->response ) ){
    	$gotchi_updates_theme_count = count( $update_themes->response );
  }
 		$update_wordpress = get_core_updates( array('dismissed' => false) );
  if ( ! empty( $update_wordpress ) && ! in_array( $update_wordpress[0]->response, array('development', 'latest') ) && current_user_can('update_core') ){
  		$gotchi_updates_core_count = 3;
  }	

return $gotchi_updates_plugin_count + $gotchi_updates_core_count + $gotchi_updates_theme_count;
		
	}
	
	/**
	* RETRIEVE DETAILED UPDATE DATA
	*
	* @since 1.1.0
	*
	* @return true
	*/
	function get_update_details() {
		
		$return = array();
		
		$update_plugins = get_site_transient( 'update_plugins' );
		if (!empty($update_plugins->response) && is_array($update_plugins->response) )  {
			
			$return['plugins']=array();
			
			foreach ($update_plugins->response as $plugin => $data) {
				$return['plugins'][] = $data->slug;
			}
		}
		
 		$update_themes = get_site_transient( 'update_themes' );
 		if (!empty($update_themes->response) && is_array($update_themes->response) )  {
			
			$return['themes']=array();
			
			foreach ($update_themes->response as $theme => $data) {
				$return['themes'][] = $data['theme'];
			}
		}
 		
 		
 		$update_wordpress = get_core_updates( array('dismissed' => false) );
 		if (!empty($update_wordpress[0]->response)) {
	 		 
	 		 $return['core']='WordPress Core needs updating!';
	 		 
	 		 }
	 	
	 	return $return;
	 			

		
	}
	
	
	/**
	* Weekly EMAIL WHEN WAPUU IS SICK
	*
	* @since 1.0.0
	*
	* @return true
	*/
	function weekly_email() {

	$gotchi_updates_total = $this->get_updates();
	
	if ( $gotchi_updates_total >= 1 ) {	 	

		if ( get_option('wapaemail') != '' ) {
			
			
			$updatedeets = $this->get_update_details();

			
			ob_start();
			
			include_once(plugin_dir_path(__FILE__) . 'templates/emailtemplate.php');
			
			$message = ob_get_contents();
			
			ob_end_clean();
		
	wp_mail( get_option('wapaemail'), 'Uh oh! Wapuu is feeling unwell!', $message,  $headers = 'Content-Type: text/html; charset=UTF-8' );
	   
    }
	}

}
	
	/**
	* DISPLAY THE VARIOUS WAPUU IMAGES
	*
	* @since 1.0.0
	*
	* @return true
	*/
	function plugin_display() {

		$gotchi_updates_total = $this->get_updates();
			switch ( true ) {
	
				case $gotchi_updates_total >= 10: $this->image('dead', 'Wapuu feeling very unwell because you have a lot of pending updates');
				break; 
				
				case $gotchi_updates_total >= 5: $this->image('verysick', 'Wapuu feeling very unwell because you have a lot of pending updates');
				break;  
				
				case $gotchi_updates_total >= 3: $this->image('sick', 'Wapuu is feeling unwell because you have a few pending updates');
				break;  
				
				case $gotchi_updates_total >= 1: $this->image('sad', 'Wapuu is feeling sad, is it time to update your site?');
				break;  
				
				case $gotchi_updates_total >= 0: $this->image('happy', 'Wapuu is feeling happy and healthy');
				break;  		
	}	
}
	
	/**
	* DISPLAYS WapuuDashboardPet
	*
	* @since 1.0.0
	*
	* @param string $status image name
	* @return echo HTML image tag
	*/
	function image($status, $alttext){
	    
		echo '<img src="' . plugins_url() . '/wapuu-dashboard-pet/images/' . $status . '.png" title="' . $alttext . '" width="200" style="position: absolute; right:0px; padding: 20px;">';
	 
    
	}	
}



$WapuuDashboardPet = new WapuuDashboardPet();
add_action('wp_loaded', array($WapuuDashboardPet,'load'));

add_action( 'upgrader_process_complete', array($WapuuDashboardPet, 'cron_activation'), 10, 2 );

register_activation_hook(__FILE__, array($WapuuDashboardPet, 'cron_activation') );
register_deactivation_hook(__FILE__, array($WapuuDashboardPet, 'cron_deactivation') );

 

 
?>