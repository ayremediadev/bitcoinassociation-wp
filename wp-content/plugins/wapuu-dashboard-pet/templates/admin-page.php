<?php

echo '<h1>' . esc_html__( 'Wapuu Dashboard Pet', 'wapuudp' ) . '</h1>';

echo '<p>' . esc_html__( 'Wapuu is your friendly dashboard virtual pet who monitors the health of your site by making sure your backups are ran and WordPress is kept up to date.', 'wapuudp' ) . '</p>';

echo '<h2>' . esc_html__( 'Email notifications', 'wapuudp' ) . '</h2>';

echo '<p>' . esc_html__( 'You can enable notifications from Wapuu which will send you a daily email if they are not feeling well. This allows you to keep on top of your updates easier. To disable updates simply clear your email from the field.', 'wapuudp' ) . '</p>';

echo '<p>' . esc_html__( 'If Wapuu needs to get in touch, what email address would you like to be contacted on?', 'wapuudp' ) . '</p>';

?>
	

<form method='post' action='options.php'>
	
<?php settings_fields( 'WDPet-email' ); ?>
<?php do_settings_sections( 'WDPet-email' ); ?>
	
<input type='email' name='wapaemail' value='<?php echo get_option('wapaemail'); ?>' />


<?php submit_button(__('Save settings', 'wapuudp')) ?>
	
	
