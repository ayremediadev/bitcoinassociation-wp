=== Wapuu Dashboard Pet ===
Contributors: kayleighthorpe
Tags: wapuu, dashboard, pet, plugin, updates
Requires at least: 4.0 or above.
Tested up to: 4.9.8
Requires PHP: 5.3 or above.
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Wapuu Dashboard Pet is a virtual pet for your WordPress admin dashboard. The more you fail to update your site, the sicker Wapuu becomes. Keep WordPress Core, plugins and themes up to date to keep them happy.

== Description ==

Wapuu Dashboard Pet is a virtual pet for your WordPress admin dashboard. The more you fail to update your site, the sicker Wapuu becomes. Keep WordPress Core, plugins and themes up to date to keep them happy.

Wapuu Dashboard Pet works by counting the number of updates available on your site. The more there are, the more unhappy they appear in the corner of the dashboard. Wapuu is a visual representation of the health of your site.


== Installation ==
1. Upload the "Wapuu Dashboard Pet" folder to the "/wp-content/plugins/" directory.
1. Activate the plugin through the "Plugins" menu in WordPress.


== Frequently Asked Questions ==
= How often will Wapuu email me? =
Once weekly if enabled.


== Changelog ==

= 1.1.3 =
* Fixed bug found with cron scheduling

= 1.1.2 =
* Updated to weekly alerts as daily was a little too much

= 1.1.1 =
* Admin page text updates and alt tags for images

= 1.0 =
* Added detailed plugin info.


