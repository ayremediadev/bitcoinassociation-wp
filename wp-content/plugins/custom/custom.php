<?php
   /*
   Plugin Name: Custom Functions
   Plugin URI: https://bitcoinassociation.net
   description: a plugin to create awesomeness and spread joy
   Version: 1.2
   Author: Mr. Awesome
   Author URI: http://mrtotallyawesome.com
   License: GPL2
   */

function create_posttype() {
 
    register_post_type( 'videos',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Videos' ),
                'singular_name' => __( 'Video' )

            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'videos'),
            'supports' => array('title', 'custom_fields')
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );


// add_action( 'pre_get_posts', 'add_my_post_types_to_query' );
 
// function add_my_post_types_to_query( $query ) {
//     if ( is_home() && $query->is_main_query() )
//         $query->set( 'post_type', array( 'post', 'videos' ) );
//     return $query;
// }
?>