<div class="header__search">
    <form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <input type="text" name="s" id="s" placeholder="Type your search...">
        <button type="submit"><i class="fas fa-search"></i></button>
    </form>
</div>