<?php get_header(); ?>
<?php
$hero_tite = get_field('hero_title')?get_field('hero_title'):"<strong>".get_the_title()."</strong>";
?>
<main id="main-content" class="bg-white">
    <div class="title__non-results">
        <div class="wrapper">
            <div class="row">
                <div class="col-8">
                    <p><strong><?php _e("Uh-oh! 404 Time! It looks like you got lost.", DOMAIN); ?></strong></p>
                </div>
            </div>
        </div>
    </div>
    <div class="page__content">
        <div class="wrapper">
            <div class="row">
                <div class="col-10">
                    <div class="page__content--main">
                        <h3><strong><?php _e("The page you are looking for could not be found.", DOMAIN); ?></strong></h3>
                        <p><?php _e("Please try searching for the content you are looking for, or check our latest news for up to date information on the cryptocurrency industry.", DOMAIN); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php get_template_part('template_parts/news__latest__page');  ?>
    <?php get_template_part('template_parts/newsletters');  ?>
</main>
<?php get_footer(); ?>

