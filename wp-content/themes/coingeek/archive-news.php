<?php
global $wp_query;
$term = get_queried_object();
$max_post = wp_count_posts('news')->publish;
?>
<?php get_header(); ?>
    <main id="main-content" class="new-list">
        <div class="category__new">
            <div class="wrapper">
                <h2 class="title"><?php _e('Latest '.$term->name.' News', DOMAIN); ?></h2>
                <div class="category__new--content">
                    <?php if (have_posts()) : ?>
                        <div class="row">
                            <?php while (have_posts()) : the_post(); ?>
                                <div class="col-3">
                                    <?php get_template_part('template_parts/new'); ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php else : ?>
                        <h2><?php _e('Nothing found', DOMAIN); ?></h2>
                    <?php endif; ?>
                </div>
                <?php if($wp_query->max_num_pages > 1): ?>
                    <div class="load__more--block">
                        <a class="btn-gradient" href="" data-tax="<?php echo $term->taxonomy; ?>" data-category="<?php echo $term->slug; ?>" data-page="1" data-maxpage="<?php echo $wp_query->max_num_pages; ?>"><span>See More</span></a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </main>
<?php get_footer(); ?>