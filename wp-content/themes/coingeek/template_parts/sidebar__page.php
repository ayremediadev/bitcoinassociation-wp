<?php
$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
$id = !empty($author)?'user_'.$author->ID:get_the_ID();
?>
<?php if((have_rows('sidebar_pages', $id)) || is_singular('contest')): ?>
<div class="page__sidebar">
    <ul>
        <?php if(is_singular('contest')): ?>
            <li>
                <p class="title__page"><?php _e('Contest register', DOMAIN); ?></p>
                <p class="link__page"><a data-fancybox="" data-src="#register__contest" href="javascrip:"><i class="fas fa-arrow-right"></i><?php _e('Register', DOMAIN); ?></a></p>
            </li>
            <li>
                <p class="title__page"><?php _e('Contest Docs', DOMAIN); ?></p>
                <p class="link__page"><a href="#" id="download__doc"><i class="fas fa-arrow-right"></i><?php _e('Download', DOMAIN); ?></a></p>
            </li>
        <?php endif; ?>
        <?php
        while(have_rows('sidebar_pages', $id)): the_row();
            $link_text = get_sub_field('page_link_text');
            $text = get_sub_field('title');
            $page = get_sub_field('page');
            ?>
            <li>
                <p class="title__page"><?= $text; ?></p>
                <p class="link__page"><a href="<?php the_permalink($page->ID); ?>"><i class="fas fa-arrow-right"></i><?= $link_text; ?></a></p>
            </li>
            <?php
        endwhile;
        ?>
    </ul>
</div>
<?php endif; ?>