<?php
    wp_reset_query();
    $title = get_field('title_last_news');
    $advertising_image = get_field('image', 'option');
    $advertising_url = get_field('link', 'option');
    $number_posts = 4;
    $classes = 'col-3';
    if(is_front_page() ){
        $number_posts = 3;
    }
    $number_posts = is_front_page()?3:4;
    $classes = is_front_page()?"col-4":"col-3";
    $classes_wrapper = is_front_page()?"col-9":"col-12";
?>
<div class="news__section">
    <div class="row">
        <div class="latest__new--left <?php echo $classes_wrapper; ?>">
            <h2 class="title__section"><?php echo $title; ?></h2>
            <div class="row">
                <?php
                    wp_reset_query();
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => $number_posts,
                        'orderby' => 'date'
                    );
                    $query = get_posts($args);
                    if(count($query)):
                        foreach ( $query as $post ) :setup_postdata( $post );
                            set_query_var( 'img_new_size', IMG_LATEST_NEWS );
                ?>
                            <div class="<?php echo $classes; ?>">
                                <?php get_template_part('template_parts/new'); ?>
                            </div>
                        <?php endforeach; ?>
                <?php endif; wp_reset_query(); ?>
            </div>
        </div>
        <?php if(is_front_page()): ?>
        <div class="latest__new--right col-3">
            <div class="include-rigister cg__radius">
                <?php if(!empty($advertising_image)): ?>
                    <a href="<?php echo $advertising_url; ?>"><img src="<?php echo $advertising_image['url'] ?>" alt=""></a>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>