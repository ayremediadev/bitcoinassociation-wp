<?php
    if(have_rows('resources')):
?>
<div class="resource__list">
    <div class="wrapper">
        <div class="row">
            <?php
                while(have_rows('resources')): the_row();
                    $option = get_sub_field('resources_icon_choose');
                    $image = get_sub_field('resource_image');
                    $icon = get_sub_field('resources_icon');
                    $page = get_sub_field('page');
                    $title = get_sub_field('title');
                    $content = get_sub_field('content');
                    $link_text = get_sub_field('link_text');
            ?>
                <div class="col-4">
                    <div class="resource__list--item">
                        <?php if($option == 'svg'): ?>
                        <div class="icon">
                             <i class="<?= $icon ?>"></i>
                        </div>
                            <?php else: ?>
                            <div class="image">
                                <img src="<?php echo $image['url']; ?>" alt="">
                            </div>
                        <?php endif; ?>
                        <h2><?= $title ?></h2>
                        <div class="content">
                            <?= $content ?>
                        </div>
                        <p class="read__more"><?= $link_text ?></p>
                        <a href="<?php echo $page; ?>" target="_blank"><?php _e('read more', DOMAIN); ?></a>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<?php endif; ?>