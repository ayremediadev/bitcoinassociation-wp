<?php
 $shortcode = get_field('form_register_contest_shortcode', 'option');
?>
<div id="register__contest">
    <div class="box_title">
        <h2 class="title__register"><?php _e("Registration", DOMAIN); ?></h2>
        <span data-fancybox-close >x</span>
    </div>
    <div class="wrapper">
        <?php echo do_shortcode($shortcode); ?>
    </div>
</div>