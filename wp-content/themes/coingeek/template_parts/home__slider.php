<div class="featuredslider__wrapper">
<?php
    wp_reset_query();
    $slider_items = get_field('slider_items');
if(!empty($slider_items)):
?>
<?php 
	$logo_img = get_field('at_logo', 'option');
?>
<div class="wrapper">
    <div class="logo-tagline__section">
        <div class="row">
            <div class="col-6 nopaddingbottom divider bitcoinbeat__logo__section"><img src="<?php echo $logo_img; ?>" alt="<?= get_bloginfo('name'); ?>" class="bitcoinbeat__logo"></div>
            <div class="col-6 nopaddingbottom bitcoinbeat__tagline__section"><h1 class="bitcoinbeat__tagline"><?= get_bloginfo('description'); ?></h1></div>
        </div>
    </div>
    <div class="slider">
        <?php
        foreach($slider_items as $item):
            set_query_var( 'img_new_size', IMG_SLIDER_HOME );
            $id = $item->ID;
            $post_title = get_the_title($id);
            $post_url = get_permalink($id);
            //$post_description = wp_trim_words( $item->post_content, 30, ' ...' );
            $post_description = get_post_meta($item->ID, '_yoast_wpseo_metadesc', true);

            $image_url = wp_get_attachment_image_src( get_post_thumbnail_id($id), $img_new_size );
            $terms = get_the_terms($id, 'category');

            $date = human_time_diff( get_the_time( 'U', $id), current_time( 'timestamp' ) ).' '.__( 'ago' );
            $date_actual = get_the_time( 'U' );


            // $post_date = get_the_time('U', $id);
            // $delta = time() - $post_date;
            // if ( $delta < 60 ) {
            //     $actual = 'Just Now';
            // }
            // elseif ($delta > 60 && $delta < 120){
            //     $actual = '1 minute ago';
            // }
            // elseif ($delta > 120 && $delta < (60*60)){
            //     $min = strval(round(($delta/60),0));
            //     $min_str = ' minutes ago';
            //     $actual = $min . $min_str;
            // }
            // elseif ($delta > (60*60) && $delta < (120*60)){
            //     $actual = 'About an hour ago';
            // }
            // elseif ($delta > (120*60) && $delta < (24*60*60)){
            //     $actual = strval(round(($delta/3600),0)), ' hours ago';
            // }
            // else {
            //     $actual = the_time('j\<\s\u\p\>S\<\/\s\u\p\> M y g:i a');
            // }
            
        ?>
            <div class="slider__wrapper">
                <div class="slider__item">
                    <div class="slider__item--content cg__radius">
                        <div class="slider__item--main">
                            <p class="new__info">
                                <span class="new__info--title"><?php echo $terms[0]->name; ?></span> <span class="new__info--date"><?php echo timeago_cg(get_the_time( 'U', $id)); ?></span>
                            </p>
                            <h2 class="title__new"><?php echo $post_title; ?></h2>
                            <div class="description">
                                <?php echo $post_description; ?>
                            </div>
                            <a href="<?php echo $post_url; ?>"><?php _e("See More", DOMAIN); ?></a>
                        </div>
                    </div>
                    <div class="slider__item--image">
                        <img src="<?php echo $image_url[0]; ?>" alt="<?php echo $post_title; ?>">
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>
</div>