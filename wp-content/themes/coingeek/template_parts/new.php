<?php
	$post_title = get_the_title();
	$post_url = get_permalink();
	$post_description = get_the_excerpt();
	if(empty($img_new_size)){
		$img_new_size = IMG_NEW_NORMAL;
	}
	$image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $img_new_size );
	if(empty($image_url)){
		$image = get_field('image_default', 'option');
		$image_url[0] = $image['sizes'][$img_new_size];
	}
	$date = human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ).' '.__( 'ago' );
	$date_actual = get_the_time( 'U' );
	$video = false;
	$terms = get_the_terms($post->ID, 'category');
	$post_type = get_post_type($post->ID);
	if($post_type == 'video'){
		$terms = get_the_terms($post->ID, 'video_category');
	}
	$content = get_the_content();
	$content = strip_tags($content);
	//$exerpt = wp_trim_words( get_the_content(), 35, ' ...' );
	$exerpt = get_post_meta($post->ID, '_yoast_wpseo_metadesc', true);
	if(preg_match("/\p{Han}+/u", $content)){
		$exerpt = wp_trim_words( get_the_content(), 2, ' ...' );
	}
?>
<div class="new">
	<div class="image cg__radius">
		<img src="<?php echo $image_url[0]; ?>" alt="<?= $post_title ?>">
		<?php if($post_type == 'video' && !empty($image_url)): ?>
			<span class="icon-Dropdown"></span>
		<?php endif; ?>
	</div>
	<div class="new__content">
		<p class="new__info">
			<span class="new__info--title"><?php if(!empty($terms)) echo $terms[0]->name; ?></span> <span class="new__info--date"><?php echo cg_time_ago(); ?></span>
		</p>
		<p class="title__new"><?php echo $post_title; ?></p>
		<p class="description"><?php echo $exerpt; ?></p>
	</div>
	<a href="<?php echo $post_url; ?>"><?php _e('Read More', DOMAIN); ?></a>
</div>


