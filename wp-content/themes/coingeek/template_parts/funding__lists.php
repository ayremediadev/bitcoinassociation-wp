<?php
    $funding_title = get_field('funding_title');
    $funding_text_link = get_field('funding_text_link');
    $funding_items = get_field('funding_items');
    $need_funding_link = get_field('need_funding_link');
?>
<div class="block-list-item-funding-list">
    <div class="wrapper">
        <h3><?= $funding_title; ?></h3>
        <div class="row">
            <?php
                foreach($funding_items as $item):
                    $funding_item_title = get_the_title($item->ID);
                    $funding_item_link = get_the_permalink($item->ID);
                    $tertiary_title = get_field('tertiary_title', $item->ID);
                    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), IMG_FUNDING_NORMAL );
            ?>
            <div class="col-6 col-lg-3">
                <div class="item">
                    <div class="image">
                        <img src="<?= $image_url[0]; ?>" alt="<?= $funding_item_title; ?>">
                    </div>
                    <div class="info">
                        <p><?= $tertiary_title ?></p>
                        <h4><?= $funding_item_title; ?></h4>
                        <span class="more"><?php _e('Learn More', DOMAIN) ?></span>
                    </div>
                    <a href="<?= $funding_item_link; ?>"><?php _e('Learn More', DOMAIN) ?></a>
                </div>
            </div>
            <?php
                endforeach;
            ?>
        </div>
        <a class="btn-gradient btn-gradient-purple" href="<?= get_the_permalink($need_funding_link->ID); ?>"><span><?= $funding_text_link; ?></span></a>
    </div>
</div>