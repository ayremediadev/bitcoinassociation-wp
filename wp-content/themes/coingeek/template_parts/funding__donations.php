<?php
    $donations_title = get_field('donations_title');
    $donations_text = get_field('donations_text');
    $donations_text_bottom = get_field('donations_text_bottom');
    $donations_text_link = get_field('donations_text_link');
    $donations_link = get_field('donations_link');
?>
<div class="block-our-donations">
    <div class="bg-block"></div>
    <div class="wrapper">
        <h3><?= $donations_title; ?></h3>
        <div class="donation__text">
            <?= $donations_text ?>
        </div>
        <a class="more" href=""><?php _e('View all our donations', DOMAIN); ?></a>
        <?php get_template_part('template_parts/donations__section'); ?>
        <p class="text-bottom"><?= $donations_text_bottom; ?></p>
        <a class="btn-gradient btn-gradient-purple" href="<?= get_the_permalink($donations_link->ID); ?>"><span><?= $donations_text_link; ?></span></a>
    </div>
</div>