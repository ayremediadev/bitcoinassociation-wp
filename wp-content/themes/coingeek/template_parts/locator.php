<?php $hero_title = get_field('hero_title'); ?>
<div class="block-locator">
   <?php $img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), IMG_LOCATOR );
   ?> 
    <div class="wrapper">
        <div class="image-top">
            <img src="<?=$img[0] ?>" alt="">
        </div>
        <div class="content">
            <h2><?= $hero_title; ?></h2>
            <div class="content__main">
                <?= $post->post_content; ?>
            </div>
        </div>
        <div class="fillter-map">
            <div class="ifram">
                <?= get_field("locator_ifram"); ?>
            </div>
        </div>
    </div>
</div>
