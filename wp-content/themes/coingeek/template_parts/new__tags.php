<?php
$tags = wp_get_post_tags(get_the_ID());
if(!empty($tags)):
?>
<div class="new__tags">
    <div class="wrapper">
        <div class="new__tags--container">
            <h2 class="title"><?php _e("tags", DOMAIN); ?></h2>
            <div class="new__tags--contents">
                <ul>
                    <?php
                    foreach($tags as $key => $item):
                        ?>
                        <li><a href="<?php echo get_term_link($item->term_id, 'post_tag'); ?>"><?php echo $item->name; ?></a></li>
                        <?php
                    endforeach
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>