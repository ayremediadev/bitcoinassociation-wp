<?php
    wp_reset_postdata();
    $term_id = get_field('video_articles');
    $term = get_term( $term_id, 'category');
    if(!empty($term_id)):
?>
<div class="video__section">
    <div class="title">
        <h2 class="title__section"><?php _e("Videos", DOMAIN); ?></h2>
        <a href="<?php echo get_post_type_archive_link('video'); ?>"><?php _e('All Videos', DOMAIN); ?></a>
    </div>
    <div class="row">
        <div class="col-8">
            <div class="business__container">
                <div class="row">
                    <?php
                    $args = array(
                        'post_type' => 'video',
                        'posts_per_page' => 2
                        // Rest of your arguments
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
                        while($query->have_posts()): $query->the_post();
                        set_query_var( 'img_new_size', IMG_NEW_NORMAL );
                    ?>
                            <div class="col-6">
                                <?php get_template_part('template_parts/new'); ?>
                            </div>
                    <?php
                        endwhile;
                    endif;
                    wp_reset_query();
                    ?>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="sidebar">
                <div class="row">
                    <?php
                    $args = array(
                        'post_type' => 'video',
                        'posts_per_page' => 4,
                        'offset' => 2,
                        // Rest of your arguments
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
                        while($query->have_posts()): $query->the_post();
                            set_query_var( 'img_new_size', IMG_BUSINESS_THUMB );
                            ?>
                            <div class="col-12">
                                <?php get_template_part('template_parts/new'); ?>
                            </div>
                            <?php
                        endwhile;
                    endif;
                    wp_reset_query();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>