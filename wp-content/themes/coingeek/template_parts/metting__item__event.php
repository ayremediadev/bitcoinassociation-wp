<div class="metting__section--content">
    <div class="wrapper">
        <div class="row">
            <?php
            $args = array(
                'post_type' => 'event',
                'posts_per_page' => -1,
                'orderby' => 'date'
            );
            $query = new WP_Query($args);
            if($query->have_posts()):
                while($query->have_posts()): $query->the_post();
                    ?>
                    <div class="col-3">
                        <?php get_template_part('template_parts/metting__item'); ?>
                    </div>
                <?php endwhile; ?>
            <?php endif; wp_reset_query(); ?>
        </div>
    </div>
</div>