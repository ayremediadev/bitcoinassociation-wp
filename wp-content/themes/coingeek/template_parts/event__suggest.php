<?php
    $title = get_field('suggest_event_title', 'option');
    $message = get_field('suggest_event_message', 'option');
    $link = get_field('suggest_event_link', 'option');
?>
<div class="wrapper">
    <div class="include__sugget--event  cg__radius">
        <div class="row">
            <div class="col-6">
                <h2 class="title__suggest"><?php echo $title; ?></h2>
                <p class="description"><?php echo $message; ?></p>
            </div>
            <div class="col-6">
                <div class="botton">
                    <a href="<?php echo $link; ?>"><?php _e('Suggest an event', DOMAIN); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
