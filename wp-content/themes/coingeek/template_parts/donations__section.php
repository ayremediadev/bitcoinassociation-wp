<?php
    $donations = get_field('donations_items');
?>
<?php if(!empty($donations)): ?>
    <div class="donations__section block-our-donations">
        <div class="row">
            <?php
            foreach($donations as $item):
                $title = $item->post_title;
                $content = $item->post_content;
                $website_link = get_field('website_link', $item->ID);
                $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), IMG_ICON_DONATION );
                ?>
                <div class="col-md-4">
                    <div class="donation__item">
                        <div class="bg-item"></div>
                        <div class="image">
                            <img src="<?php echo $image_url[0]; ?>" alt="<?php echo $title; ?>">
                        </div>
                        <div class="info">
                            <h4><?php echo $title; ?></h4>
                            <?php echo $content;  ?>
                            <a class="link" href="<?php echo $website_link; ?>" target="_blank"><?php _e('Visit website', DOMAIN) ?></a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>