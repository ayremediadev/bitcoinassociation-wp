<?php
    $title = get_the_title();
    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ) );
    $website_link = get_field('website');
    $countries = get_the_terms($post->ID, 'exchange_country');
    $countries = merge_string_terms($countries);
    $currencies = get_the_terms($post->ID, 'exchange_currency');
    $currencies = merge_string_terms($currencies);
    $device = get_the_terms($post->ID, 'exchange_devices');
    $device = merge_string_terms($device);
    $exchange_funds = get_the_terms($post->ID, 'exchange_funds');
    if(!empty($exchange_funds)):
    $exchange_funds = merge_string_terms($exchange_funds);
    endif;
?>
<div class="col-3">
    <div class="box__results cg__radius">
        <div class="title__results">
            <h3><?php echo $title; ?></h3>
            <div class="image">
                <img src="<?php echo $image_url[0]; ?>" alt="A">
            </div>
        </div>
        <div class="item__results">
            <h4><?php _e("Countries", DOMAIN) ?></h4>
            <span><?php echo $countries; ?></span>
        </div>
        <div class="item__results">
            <h4><?php _e("Currencies", DOMAIN) ?></h4>
            <span><?php echo $currencies; ?></span>
        </div>
        <?php
            if(!empty($exchange_funds)):
        ?>
        <div class="item__results">
            <h4><?php _e("Limit", DOMAIN) ?></h4>
            <span><?php echo $exchange_funds; ?></span>
        </div>
        <?php
            endif;
        ?>
        <div class="item__results">
            <h4><?php _e("Device / OS", DOMAIN) ?></h4>
            <span><?php echo $device; ?></span>
        </div>
        <?php if(!empty($website_link)): ?>
        <span class="link">Visit Website</span>
        <?php endif; ?>
        <?php if(!empty($website_link)): ?>
            <a class="link__box cg__radius" href="<?php echo $website_link; ?>" target="_blank"></a>
        <?php endif; ?>
    </div>
</div>
