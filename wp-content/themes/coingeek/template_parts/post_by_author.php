<?php
	wp_reset_postdata();
	$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
	$id = $author->ID;
	$name = $author->display_name;
?>
<?php
$args = array(
	'post_type' => 'post',
	'author'        =>  $id,
	'orderby'       =>  'post_date',
	'order'         =>  'DESC',
	'posts_per_page' => -1
);
$current_user_posts = new WP_Query($args);
?>
<?php  if ( $current_user_posts->have_posts() ) : ?>
<div class="block__article--author">
	<div class="wrapper">
		<div class="latest__new--left post__author">
			<h2 class="title__section"><?php _e("articles by ".$name); ?></h2>
			<div class="row">
				<?php
				while ( $current_user_posts->have_posts() ) : $current_user_posts->the_post(); ?>
					<div class="col-4">
						<?php get_template_part('template_parts/new'); ?>
					</div>
				<?php
				endwhile;
				?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>