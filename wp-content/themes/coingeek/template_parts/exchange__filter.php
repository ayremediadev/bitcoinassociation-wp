<?php
    $countries = get_terms( array(
        'taxonomy' => 'exchange_country',
        'hide_empty' => false,
    ) );
    $currencies = get_terms( array(
        'taxonomy' => 'exchange_currency',
        'hide_empty' => false,
    ) );
    $device = get_terms( array(
        'taxonomy' => 'exchange_devices',
        'hide_empty' => false,
    ) );
    $tool = !empty(get_field('object_tool'))?get_field('object_tool'):'exchange';
?>
<div class="box__filter">

    <div class="box">
        <h3><?php _e("Location", DOMAIN); ?></h3>
        <div class="location">
            <?php echo facetwp_display( 'facet', 'location' ); ?>
        </div>
    </div>
    <div class="box">
        <h3><?php _e("Currency", DOMAIN); ?></h3>
        <div class="currency">
            <?php echo facetwp_display( 'facet', 'currency' ); ?>
        </div>
    </div>
    <div class="box">
        <h3><?php _e("Device & OS", DOMAIN); ?></h3>
        <div class="device">
            <?php echo facetwp_display( 'facet', 'device_type' ); ?>
        </div>
    </div>
    <div class="botton__see--web apply">
        <a class="btn-gradient" href="#" ><span><?php _e("APPLY", DOMAIN); ?></span></a>
    </div>
    <div class="reset">
        <button onclick="FWP.reset()"><?php _e("RESET FILTERS", DOMAIN); ?></button>
    </div>
</div>