<?php
    if(is_singular('post') || is_search() || is_author()){
        $classes = 'header__post';
    }else if(is_singular('event')){
        $classes = 'header__event';
    }
?>
<div class="header__menu <?php if(!empty($classes)): echo $classes; endif;?>">
    <?php
    if ( has_nav_menu( 'athena_main_menu' ) ) {
        wp_nav_menu( array(
            'theme_location' => 'main_nav',
            'menu' => 'header_menu',
            'menu_class' => '',
            'container' => '',
            'link_before' => '',
            'link_after'  => '<span class="plus_icon"></span>'
        ) );
    }
    ?>
</div>