<?php
    wp_reset_query();
    $sidebar_images = get_field('image_sidebar');
    $sidebar_link = empty(get_field('image_sidebar_link'))?"#":get_field('image_sidebar_link');
?>
<div class="news__section">
    <div class="row">
        <div class="latest__new--left col-9">
            <h2 class="title__section"><?php _e('latest news', DOMAIN); ?></h2>
            <div class="row">
                <?php
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => 3,
                        'orderby' => 'date'
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
                        while($query->have_posts()): $query->the_post();
                            set_query_var( 'img_new_size', IMG_LATEST_NEWS );
                ?>
                            <div class="col-4">
                                <?php get_template_part('template_parts/new'); ?>
                            </div>
                        <?php endwhile; ?>
                <?php endif; wp_reset_query(); ?>
            </div>
        </div>
        <div class="latest__new--right col-3">
            <div class="include-rigister cg__radius">
                <a href="<?php echo $sidebar_link; ?>">
                    <img src="<?php echo $sidebar_images['url']; ?>" alt="">
                </a>
            </div>
        </div>
    </div>
</div>