<div class="comments__new">
    <div class="commentlist">
        <h2 class="title"><?php _e("COMMENT", DOMAIN); ?></h2>
        <?php
        $comments = get_comments(array(
            'post_id' => $post->ID));
        $args = array(
            'walker'            => null,
            'max_depth'         => '',
            'style'             => 'ul',
            'callback'          => 'athena_comment',
            'end-callback'      => null,
            'type'              => 'all',
            'reply_text'        => 'Reply',
            'page'              => '',
            'per_page'          => '',
            'avatar_size'       => 32,
            'reverse_top_level' => null,
            'reverse_children'  => '',
            'format'            => 'html5', // or 'xhtml' if no 'HTML5' theme support
            'short_ping'        => false,   // @since 3.6
            'echo'              => true     // boolean, default is true
        );
        wp_list_comments( $args, $comments); ?>
        <div class="form__comment">
            <div id="respond">

                <h2><?php _e("Add a Comment")?></h2>

                <div class="cancel-comment-reply">
                    <?php cancel_comment_reply_link(); ?>
                </div>

                <?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
                    <p>You must be <a href="<?php echo wp_login_url( get_permalink() ); ?>">logged in</a> to post a comment.</p>
                <?php else : ?>

                    <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

                        <?php if ( is_user_logged_in() ) : ?>

                            <p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out &raquo;</a></p>

                        <?php else : ?>

                            <div>
                                <input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" placeholder="Name" size="22" tabindex="1" required="required" <?php if ($req) echo "aria-required='true'"; ?> />
                            </div>

                            <div>
                                <input type="email" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" placeholder="Email" size="22" tabindex="2" required="required" <?php if ($req) echo "aria-required='true'"; ?> />
                            </div>

                            <div>
                                <input type="text" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" placeholder="Website" size="22" tabindex="3" />
                            </div>

                        <?php endif; ?>

                        <!--<p>You can use these tags: <code><?php echo allowed_tags(); ?></code></p>-->

                        <div>
                            <textarea name="comment" id="comment" cols="58" rows="10" tabindex="4" placeholder="Comment" required></textarea>
                        </div>

                        <div>
                            <button type="submit" class="btn-gradient">
                                <span>Comment</span>
                            </button>
                            <?php comment_id_fields(); ?>
                        </div>

                        <?php do_action('comment_form', $post->ID); ?>

                    </form>

                <?php endif; // If registration required and not logged in ?>

            </div>
        </div>
    </div>
</div>