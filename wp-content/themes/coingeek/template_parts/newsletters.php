<?php
  $short_code = get_field('shortcode', 'option');
?>
<div class="box__newsletter">
  <div class="wrapper">
    <div class="row">
      <div class="include__newsletter">
        <?php echo do_shortcode($short_code); ?>
      </div>
    </div>
  </div>
</div>