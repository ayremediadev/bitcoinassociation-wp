<?php
    wp_reset_postdata();
    $term_id = get_field('technology_articles');
    $term = get_term( $term_id, 'category');
if(!empty($term_id)):
?>
<div class="bicoin__section">
    <div class="title">
        <h2 class="title__section"><?php echo $term->name; ?></h2>
        <a href="<?php echo get_category_link($term_id); ?>"><?php _e('All '.$term->name.' Articles', DOMAIN); ?></a>
    </div>
    <div class="row">
        <div class="col-8">
            <div class="business__container">
                <div class="row">
                    <?php
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => 2,
                        'tax_query' => [
                            [
                                'taxonomy' => 'category',
                                'terms' => $term_id
                            ],
                        ],
                        // Rest of your arguments
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
                        while($query->have_posts()): $query->the_post();
                            set_query_var( 'img_new_size', IMG_NEW_NORMAL );
                            ?>
                            <div class="col-6">
                                <?php get_template_part('template_parts/new'); ?>
                            </div>

                            <?php
                        endwhile;
                    endif;
                    wp_reset_query();
                    ?>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="sidebar">
                <div class="row">
                    <?php
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => 4,
                        'offset' => 2,
                        'tax_query' => [
                            [
                                'taxonomy' => 'category',
                                'terms' => $term_id
                            ],
                        ],
                        // Rest of your arguments
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
                        while($query->have_posts()): $query->the_post();
                            set_query_var( 'img_new_size', IMG_BUSINESS_THUMB );
                            ?>
                            <div class="col-12">
                                <?php get_template_part('template_parts/new'); ?>
                            </div>
                            <?php
                        endwhile;
                    endif;
                    wp_reset_query();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>