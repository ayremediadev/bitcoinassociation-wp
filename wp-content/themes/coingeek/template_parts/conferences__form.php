<?php
    $mainTitle = get_field('main_title');
    $datetime = get_field('date_time');
    $second_title = get_field('second_title');
    $shortCode = get_field('shortcode_form');
?>
<div id="register" class="block-contact-form">
    <div class="wrapper">
        <div class="heading">
            <p class="main-title"><?= $mainTitle; ?></p>
            <p class="date-time"><?= $datetime; ?></p>
            <p class="des"><?= $second_title; ?></p>
        </div>
        <div class="form-default">
            <?php echo do_shortcode($shortCode); ?>
        </div>
    </div>
</div>