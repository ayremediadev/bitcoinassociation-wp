<div class="button-mobile">
    <span>CG</span>
</div>
<i class="icons-search fas fa-search "></i>
<div class="header__social">
    <ul>
        <?php
            if( have_rows('socials', 'option') ):
                while ( have_rows('socials', 'option') ) : the_row();
                    $link = get_sub_field('social_link', 'option');
                    $class = get_sub_field('social_class', 'option');
        ?>
        <li>
            <a href="<?php echo $link; ?>" target="_blank"><i class="<?php echo $class; ?>"></i></a>
        </li>
        <?php
                endwhile;
            endif;
        ?>
    </ul>
</div>