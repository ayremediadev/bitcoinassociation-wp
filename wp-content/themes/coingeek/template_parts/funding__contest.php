<?php
    $contest_title = get_field('contest_title');
    $contest_text = get_field('contest_text');
    $contest_object = get_field('contest_object');
    $contest_title = get_the_title($contest_object->ID);
    $secondary_title = get_field('secondary_title', $contest_object->ID);
    $deadline = get_field('deadline', $contest_object->ID);
?>
<div class="block-our-current-contest">
    <div class="bg-img bg" style="background-image: url(../wp-content/themes/coingeek/assets/images/bg-banner.png)"></div>
    <div class="wrapper">
        <h3><?= $contest_title; ?></h3>
        <div class="contest__text">
            <?= $contest_text; ?>
        </div>
        <div class="box-info">
            <div class="reward">
                <p><?= $deadline; ?></p>
            </div>
            <h4><?= $contest_title; ?></h4>
            <p><?= $secondary_title; ?></p>
            <a class="btn-gradient btn-gradient-purple" data-fancybox="" data-src="#register__contest" href="javascrip:"><span><?php _e("Register", DOMAIN); ?></span></a>
        </div>
    </div>
</div>