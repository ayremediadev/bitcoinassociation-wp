<?php
    $hero_tite = get_field('hero_title')?get_field('hero_title'):"";
    $title = get_the_title();
    $content = $post->post_content;
    $image = get_field('funding_background');
?>
<div class="block-banner-funding-list bg" style="background-image: url(<?= $image['url']; ?>)">
    <div class="wrapper">
        <?php
            if(!empty($hero_tite)):
        ?>
        <h5><?= $hero_tite ?></h5>
        <?php endif; ?>
        <h2><?= $title; ?></h2>
        <?= $content; ?>
    </div>
</div>