<div class="news__latest__page">
    <div class="wrapper">
        <div class="row">
            <div class="latest__new--left col-12">
                <h2 class="title__section"><?php _e('latest news', DOMAIN); ?></h2>
                <div class="row">
                    <?php
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => 3,
                        'orderby' => 'date'
                    );
                    $query = get_posts($args);
                    if(count($query)):
                        foreach ( $query as $post ) :setup_postdata( $post );
                            set_query_var( 'img_new_size', IMG_LATEST_NEWS );
                            ?>
                            <div class="col-4">
                                <?php get_template_part('template_parts/new'); ?>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </div>
</div>