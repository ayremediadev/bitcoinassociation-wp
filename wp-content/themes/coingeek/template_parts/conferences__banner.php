<?php
    $hero_tite = get_field('hero_title')?get_field('hero_title'):"";
    $banner_image = get_field('banner_image')
?>
<div class="banner-inter" <?php if(!empty($banner_image)): ?>style="background-image: url('<?= $banner_image['url']; ?>')" <?php endif; ?>>
    <div class="content">
        <?php echo $hero_tite; ?>
    </div>
</div>