<?php
    $frontpage_id = get_option( 'page_on_front' );
    $banner_week = get_field('business_banner_image', $frontpage_id );
    $banner_link = get_field('business_banner_link', $frontpage_id );
    if($frontpage_id != get_the_ID()){
        $classes = "news__page";
    }else{
        $classes = "news__wrapper";
    }
?>
<div class="home__section_content white <?php echo $classes; ?>">
    <div class="wrapper">
        <?php get_template_part('template_parts/home__latestnews__section'); ?>
        <?php get_template_part('template_parts/business__section'); ?>
        <div class="banner__section banner__business">
            <?php if($banner_week): ?>
                <div class="week__banner">
                    <a href="<?php echo $banner_link; ?>"><img src="<?php echo $banner_week['url'] ?>" alt=""></a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

