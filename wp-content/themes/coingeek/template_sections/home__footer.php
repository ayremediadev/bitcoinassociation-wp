<?php
    $logo = get_field('logo_home_footer', 'option');
    $description = get_field('description', 'option');
    $short_code = get_field('shortcode', 'option');
?>
<div class="footer__home">
    <div class="wrapper">
        <div class="row">
            <div class="col-4">
                <div class="footer__home--logo">
                    <div class="logo">
                        <a href="">
                            <img src="<?php echo $logo['url']; ?>" alt="">
                        </a>
                    </div>
                </div>
                <div class="footer__home--description">
                    <?php echo $description; ?>
                </div>
            </div>
            <div class="col-4">
                <div class="footer__list--menu">
                    <?php if ( is_active_sidebar( 'home-footer' ) ) : ?>
                        <?php dynamic_sidebar( 'home-footer' ); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-4">



                <div class="footer__social">
                    <p><?php _e('Follow us', DOMAIN); ?></p>
                    <ul>
                        <?php
                            if( have_rows('socials', 'option') ):
                                while ( have_rows('socials', 'option') ) : the_row();
                                    $link = get_sub_field('social_link', 'option');
                                    $class = get_sub_field('social_class', 'option');
                                    ?>
                                    <li>
                                        <a href="<?php echo $link; ?>" target="_blank"><i class="<?php echo $class; ?>"></i></a>
                                    </li>
                                    <?php
                                endwhile;
                            endif;
                        ?>
                    </ul>
                </div>







                <div class="footer__newsletters">
                    <div class="row">
                        <?php echo do_shortcode($short_code); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>