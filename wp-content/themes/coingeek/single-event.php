<?php
    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), IMG_EVENT_DETAIL );
    $terms = get_the_terms($post->ID, 'category');
    $website_link = get_field('website_link');
    $advertising_image = get_field('image', 'option');
    $advertising_url = get_field('link', 'option');
?>
<?php get_header(); ?>
<main id="main-content">
    <section id="content__event--detail">
        <?php if($image_url): ?>
        <div class="banner">
            <div class="wrapper">
                <img src="<?php echo $image_url[0]; ?>" alt="">
            </div>
        </div>
        <?php endif; ?>
        <div class="main__content">
            <div class="wrapper">
                <div class="row">
                    <div class="col-8">
                        <?php if(!empty($terms)): ?>
                        <p class="new__info">
                            <span class="new__info--title"><?php echo $terms[0]->name; ?></span>
                        </p>
                        <?php endif; ?>
                        <div class="content">
                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <h2 class="title"><?php the_title(); ?></h2>
                            <?php the_content(); ?>
                        </div>
                        <?php endwhile; endif; ?>
                        <?php if($website_link): ?>
                            <div class="botton__see--web">
                                <a href="<?php echo $website_link; ?>" class="btn-gradient" target="_blank"><span><?php _e("See website", DOMAIN); ?></span></a>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-4">
                        <?php if(!empty($advertising_image)): ?>
                            <div class="advertising__image">
                                <a href="<?php echo $advertising_url; ?>"><img src="<?php echo $advertising_image['url'] ?>" alt=""></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php get_template_part('template_sections/home__footer'); ?>
<?php get_footer(); ?>
