<?php
/**
 * Template Name: Contact news
 */
get_header();
$hero_tite = get_field('hero_title')?get_field('hero_title'):"<strong>".get_the_title()."</strong>";
?>
<div class="page__contact-news">
	<div class="contact__news">
		<div class="banner__contact title__non-results">
			<div class="wrapper">
				<?= $hero_tite; ?>
			</div>
		</div>
		<div class="box__contact--news">
			<div class="wrapper">
				<p class="title__contact"><?php echo get_field("contact_news_title_form") ?></p>
				<div class="row">
					<?php $form = get_field('contact_news_source_code'); ?>
					<?php echo do_shortcode( $form); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_template_part('template_parts/newsletters');  ?>
<?php get_template_part('template_sections/home__footer'); ?>
<?php get_footer(); ?>
