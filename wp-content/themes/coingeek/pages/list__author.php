<?php
/**
 * Template Name: List author
 */
get_header();
?>
	<main id="main-content">
		<?php get_template_part('template_parts/content__author__list'); ?>
		<?php get_template_part('template_parts/newsletters');  ?>
		<?php get_template_part('template_sections/home__footer'); ?>
	</main>
<?php get_footer(); ?>