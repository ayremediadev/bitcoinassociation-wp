<?php
/**
 * Template Name: Contact
 */
get_header();
?>
<div class="block__contact">
	<div class="wrapper">
		<div class="row">
			<div class="text">
				<h2>Coingeek Rigs</h2>
				<p class="sub_title">The future of mining rigs</p>
				<p class="description">You will soon be able to buy our mining rigs online. Meanwhile, feel free to contact us through the form below if you are interested in making a purchase now or in the future.</p>
			</div>
			<div class="form__contact">
				<?php 
				echo do_shortcode( "[contact-form-7 id='226' title='Contact form 1']" ); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>