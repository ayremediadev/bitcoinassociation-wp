<?php
/**
 * Template Name: Event
 */
get_header();
$banner_week = get_field('banner_image');
$banner_link = get_field('banner_link');
?>
    <main id="main-content" class="page-event-list-body" data-id="<?php the_ID(); ?>">
        <?php get_template_part('template_parts/event__filter'); ?>
        <div class="include__content--filter">
            <div class="metting__section--content">
                <div class="wrapper">
                    <div class="row">
                        <?php
                            $year = date("Y");
                            $year_next = $year;
                            $month = date("m");
                            $day = date("d");
                            $args = array(
                                'post_type' => array(
                                    'event'
                                ),
                                'post_status' => array(
                                    'publish'
                                ),
                                'order' => 'ASC',
                                'orderby' => 'from_date',
                                'meta_query' => array(
                                    'relation' => 'AND',
                                    array(
                                        'key' => 'from_date',
                                        'value' => $year.$month.$day, // Lowest date value
                                        'compare' => '>='
                                    ),
                                    array(
                                        'key' => 'from_date',
                                        'value' => $year_next.$month."32", // Highest date value
                                        'compare' => '<'
                                    )
                                )
                            );
                            $query = new WP_Query($args);
                            if ($query->have_posts()) :
                         ?>
                            <?php
                            $post_idx = 0;
                            $post_count = $query->post_count;
                            while ($query->have_posts()):
                                $query->the_post(); $post_idx++;
                                ?>
                                <div class="col-3">
                                    <?php get_template_part('template_parts/metting__item'); ?>
                                </div>
                                <?php if($post_idx == 8 || ($post_idx < 8 && $post_idx == $post_count)): ?>
                                <div class="col-12">
                                    <div class="banner__section">
                                        <?php if($banner_week): ?>
                                            <div class="week__banner">
                                                <a href="<?php echo $banner_link; ?>"><img src="<?php echo $banner_week['url'] ?>" alt=""></a>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <?php endif; ?>
                            <?php endwhile; ?>

                    <?php else : ?>
                        <div class="col-12 content__not--found">
                            <h2><?php _e('Sorry, no events found', DOMAIN); ?></h2>
                        </div>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="content__not--found" style="display: none;">
                <div class="wrapper">
                    <h2><?php _e('Sorry, no events found', DOMAIN); ?></h2>
                    <a href="#"><?php _e('Reset filters', DOMAIN); ?></a>
                </div>
            </div>
            <?php get_template_part('template_parts/event__suggest'); ?>
        </div>
    </main>

<?php get_footer(); ?>