<?php
/**
 * Template Name: HardWeare
 */
get_header();
$hero_tite = get_field('hero_title')?get_field('hero_title'):"";
$bchTitle = get_field('main_title');
$secondTitle = get_field('second_title');
?>
    <div class="block__contact">
        <div class="wrapper">
            <div class="row form">
                <div class="text">
                    <?= $hero_tite; ?>
                 </div>
                <div class="form__contact">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; endif; ?>
                </div>
            </div>
            <div class="info">
                <div class="row">
                    <div class="text-info">
                        <h2><?php echo $bchTitle; ?></h2>
                        <p><?php echo $secondTitle; ?></p>
                    </div>
                    <div class="logo">
                        <?php
                            $i = 0;
                                if(have_rows('logo')):
                                    while(have_rows('logo')): the_row();
                                        $logo_image = get_sub_field('image');
                                        $link = get_sub_field('link');
                                        $textlink = get_sub_field('text_link');
                                        if(++$i % 2 == 1):
                        ?>
                        <div class="right">
                            <div class="col-6">
                                <div class="img">
                                    <img src="<?php echo $logo_image['url']; ?>" alt="">
                                </div>
                                <a href="<?php echo $link; ?>" target="_blank"><?php echo $textlink ?></a>
                            </div>
                        </div>
                        <?php
                          else:
                        ?>
                        <div class="col-6">
                            <div class="left">
                                <div class="img2">
                                    <img src="<?php echo $logo_image['url']; ?>" alt="">
                                </div>
                                <a href="<?php echo $link; ?>" target="_blank" ><?php echo $textlink ?></a>
                            </div>
                        </div>
                        <?php endif; endwhile; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>