<?php
/**
 * Template Name: Resource
 */
get_header();
?>
<?php
$hero_tite = get_field('hero_title')?get_field('hero_title'):"<strong>".get_the_title()."</strong>";
?>
<main id="main-content" class="bg-white">
    <div class="title__non-results">
        <div class="wrapper">
            <div class="row">
                <div class="col-8">
                    <?= $hero_tite; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="page__content">
        <div class="wrapper">
            <div class="row">
                <div class="col-10">
                    <div class="page__content--main resource__content">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; endif; ?>
                    </div>
                </div>
                <?php
                if(have_rows('sidebar_pages')):
                    ?>
                    <div class="col-2">
                        <?php get_template_part('template_parts/sidebar__page'); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php get_template_part('template_parts/resources__list'); ?>

    <?php if( have_rows('bch_news_dowload') ):

        while( have_rows('bch_news_dowload') ): the_row();  ?>
        <div class="block__dowload">
            <div class="wrapper">
                <div class="info">
                    <h3><?= get_sub_field("title") ?></h3>
                    <p><?= get_sub_field("sub_title") ?></p>
                </div>
                <div class="botton">
                    <div class="dowload">
                        <a href="<?= get_sub_field('link_dowload') ?>" target="_blank" download><?= get_sub_field("text_dowload") ?></a>
                    </div>
                    <?php if(!empty(get_sub_field("link_learn"))): ?>
                    <div class="learn">
                        <a href="<?= get_sub_field("link_learn") ?>" target="_blank"><?= get_sub_field("text_learn_more") ?></a>
                    </div>
                <?php endif; ?>
                </div>
            </div>
        </div>
        <?php
        endwhile;
    endif;
    ?>

    <div class="include__learn--more">
        <div class="wrapper">
            <h3><?= get_field("bch_news_title_faq") ?></h3>
            <div class="row">
                <div class="box">
                    <?php
                    $args = array(
                        'post_type' => 'faq_cpt',
                        'posts_per_page' => -1,
                        'orderby' => 'date',
                        'order' => 'ASC'
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
                        while($query->have_posts()): $query->the_post();
                    ?>
                            <div class="col-4">
                                <a href="/faq?item=<?=get_the_ID()?>"><span><?= get_the_title() ?><span class="fas fa-arrow-right"></span></span></a>
                            </div>
                            <?php
                        endwhile;
                    endif;
                  ?>
                </div>
            </div>
        </div>
    </div>
    <?php get_template_part('template_parts/newsletters');  ?>
</main>
<?php get_footer(); ?>
