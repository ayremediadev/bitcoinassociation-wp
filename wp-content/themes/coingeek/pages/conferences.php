<?php
/**
 * Template Name: Conferences
 */
get_header('conferences');
?>
    <div id="main-content" class="page-contact-temp-html">
        <?php get_template_part('template_parts/conferences__banner'); ?>
        <?php get_template_part('template_parts/conferences__location'); ?>
        <?php get_template_part('template_parts/conferences__form'); ?>
        <?php get_template_part('template_parts/newsletters');  ?>
    </div>
<?php get_footer(); ?>