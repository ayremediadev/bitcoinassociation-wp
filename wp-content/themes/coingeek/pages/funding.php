<?php
/**
 * Template Name: Funding Template
 */
?>
<?php get_header('funding'); ?>
<main id="main-content">
    <?php get_template_part('template_parts/funding__banner');  ?>
    <?php get_template_part('template_parts/funding__lists');  ?>
    <?php get_template_part('template_parts/funding__contest');  ?>
    <?php get_template_part('template_parts/funding__donations');  ?>
    <?php get_template_part('template_parts/newsletters');  ?>
</main>
<?php get_template_part('template_parts/register__contest'); ?>
<?php get_footer(); ?>
