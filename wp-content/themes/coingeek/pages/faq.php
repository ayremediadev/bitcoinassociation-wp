<?php
/**
 * Template Name: Faq
 */
get_header();
$banner_week = get_field('banner_image');
$banner_link = get_field('banner_link');
?>
<?php
$hero_tite = get_field('hero_title')?get_field('hero_title'):"<strong>".get_the_title()."</strong>";
$item_id = $_GET["item"];
?>
<main id="main-content" class="bg-white">
    <div class="title__non-results">
        <div class="wrapper">
            <div class="row">
                <div class="col-8">
                    <?= $hero_tite; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="page__content">
        <div class="wrapper">
            <div class="row">
                <div class="col-10">
                    <div class="page__content--main">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; endif; ?>
                    </div>
                    <?php
                        $args = array(
                            'post_type' => 'faq_cpt',
                            'posts_per_page' => -1,
                            'orderby' => 'date',
                            'order' => 'ASC'
                        );
                        $query = new WP_Query($args);
                        if($query->have_posts()):
                    ?>
                    <div class="faq__list">
                        <?php
                            while($query->have_posts()): $query->the_post();
                        ?>
                        <div class="item">
                            <h2 data-show="<?= get_the_ID() ?>"><?= get_the_title() ?> <i class="fas fa-caret-down"></i></h2>
                            <div id="faq__item--<?= get_the_ID() ?>" class="item__content"><?= get_the_content() ?> </div>
                        </div>
                        <?php
                            endwhile;
                        wp_reset_postdata();
                        ?>
                    </div>
                    <?php endif; ?>
                </div>
                <?php
                if(have_rows('sidebar_pages')):
                    ?>
                    <div class="col-2">
                        <?php get_template_part('template_parts/sidebar__page'); ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="banner__faq">
                        <?php if($banner_week): ?>
                            <div class="week__banner">
                                <a href="<?php echo $banner_link; ?>"><img src="<?php echo $banner_week['url'] ?>" alt=""></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php get_template_part('template_parts/resources__list'); ?>
    <?php get_template_part('template_parts/newsletters');  ?>
</main>
<?php get_footer(); ?>

<script>
    $(document).ready(function(){
        var id = '<?php echo $item_id ?>';
        $("#faq__item--"+id).slideDown().addClass('active');
        $("#faq__item--"+id).parent().find('h2').addClass('active');
        var x = $("#faq__item--"+id).offset();
        var height = x.top -200;
        setTimeout(
            function()
            {
                $('body,html').animate({ scrollTop: height  }, 1000);
                return false;
            }, 1000
        );
    });

</script>
