<?php
/**
 * Template Name: Page Wallet
 */
get_header();
$hero_tite = get_field('hero_title')?get_field('hero_title'):"<strong>".get_the_title()."</strong>";
$content = $post->post_content;
?>
    <main id="main-content">
        <div class="page__non-results">
            <div class="title__non-results">
                <div class="wrapper">
                    <?= $hero_tite; ?>
                </div>
            </div>
            <div class="include">
                <div class="main__content">
                    <div class="wrapper">
                        <div class="row">
                            <div class="col-8">
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="filter__resources">
                    <div class="wrapper">
                        <?php get_template_part('template_parts/wallet__filter'); ?>
                    </div>
                </div>
                <div class="content__resources">
                    <div class="has__results">
                        <div class="wrapper">
                            <div class="row">
                                <?php
                                echo do_shortcode('[facetwp template="wallet"]');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="non__results" style="display: none;">
                        <p><?php _e('Sorry, no resources found', DOMAIN); ?></p>
                        <div class="reset">
                            <button onclick="FWP.reset()"><?php _e("Reset filters", DOMAIN); ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <?php get_template_part('template_parts/newsletters') ?>
        </div>
    </main>

    <!--Start Pull HTML here-->
    <!--END  Pull HTML here-->
<?php get_footer(); ?>