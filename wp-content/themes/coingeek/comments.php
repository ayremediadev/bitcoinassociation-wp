<div class="comment home__section_content">
	<div class="wrapper">
		<div class="title">
              <h2 class="title__section">Comment</h2>
        </div>
        <div class="row">
        	<div class="col-9">

				<?php if (comments_open()) :?>
					<div id="disqus_thread"></div>
					<?php
					$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
					?>
					<script>
						/**
						*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
						*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
						*/
						
						var disqus_config = function () {
							this.page.url = '<?php echo get_permalink(); ?>';  // Replace PAGE_URL with your page's canonical URL variable
							this.page.identifier = '<?php echo dsq_identifier_for_post($post); ?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
						};
						
						(function() {  // DON'T EDIT BELOW THIS LINE
							var d = document, s = d.createElement('script');
							
							s.src = '//coingeek.disqus.com/embed.js';
							
							s.setAttribute('data-timestamp', +new Date());
							(d.head || d.body).appendChild(s);
						})();
					</script>
					<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
				<?php endif; // comments_open ?>


        	</div>
        </div>
	</div>
</div>
