<?php
get_header();
?>
<main id="main-content">
	<?php get_template_part('template_parts/info__author'); ?>
	<?php get_template_part('template_parts/post_by_author'); ?>
	<?php get_template_part('template_parts/newsletters');  ?>
	<?php get_template_part('template_sections/home__footer'); ?>
</main>
<?php get_footer(); ?>