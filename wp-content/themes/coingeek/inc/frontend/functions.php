<?php
function wpb_widgets_init() {

    register_sidebar( array(
        'name' => __( 'Home Footer', DOMAIN ),
        'id' => 'home-footer',
        'description' => __( 'Home Footer', DOMAIN),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => '',
    ) );
}

add_action( 'widgets_init', 'wpb_widgets_init' );

add_filter( 'wpseo_remove_reply_to_com', '__return_false' );

/**
 * merge strings in terms
 */

function merge_string_terms($terms){
    $string = '';
    if(!empty($terms)){
        foreach($terms as $key => $term){
            $string .= $term->name;
            if($key == count($terms) - 1)
                return $string;
            $string .= ", ";
        }
    }
    return $string;
}

/**
 * change number post of news post type
 */
function wpsites_query( $query ) {
    if ( ($query->is_tax() || $query->is_archive() || $query->is_post_type_archive('video')) && $query->is_main_query() && !is_admin() ) {
        $query->set( 'posts_per_page', 12 );
    }

    if (($query->is_search || is_tag()) && $query->is_main_query() && !is_admin() ) {
        $query->set( 'post_type', 'post' );
        $query->set( 'posts_per_page', 12 );
    }

    if ( ($query->is_post_type_archive('event')) && $query->is_main_query() && !is_admin() ) {
        $array = array(
            array(
                'year'  => date("Y"),
                'month' => date("m")
            ),
        );
        $query->set( 'date_query', $array);
    }
}
add_action( 'pre_get_posts', 'wpsites_query' );


/**
 * add data-title for menu
 */
add_filter( 'nav_menu_link_attributes', 'header_menu_atts', 10, 3 );
function header_menu_atts( $atts, $item, $args )
{
    $atts['data-title'] = $item->title;
    return $atts;
}

/**
 * get months English
 */
 function getMonth(){
     $months = array(
         'January',
         'February',
         'March',
         'April',
         'May',
         'June',
         'July ',
         'August',
         'September',
         'October',
         'November',
         'December',
     );
     return $months;
 }

function timeago_cg($date,$granularity=2) {
    $diff = time() - $date;
    $is_today     = date( 'Y-m-d', current_time('timestamp') ) == date( 'Y-m-d', $date );
    $is_yesterday = date( 'Y-m-d', current_time('timestamp') - 86400 ) == date( 'Y-m-d', $date );

    if( $diff  < 0 ) return;

    // less than 2 minutes - print just now or moments ago
    if( $diff < 120 ){
        $text = __('Just now', 'coingeek');

    }

    // less than one hour - print x minutes ago
    elseif( $diff < 3600 ){
        $text = sprintf( __('%d minutes ago', 'coingeek'), ( floor( $diff / 60 ) % 60 ) );

    }

    // within an hour
    elseif( $diff < 7200 ) {
        $text = sprintf( __('%d hour ago', 'coingeek'), ( floor( $diff / 60 / 60 ) % 24 ) );

    }

    // belongs to today
    elseif( $is_today ){
        $text = sprintf( __('%d hours ago', 'coingeek'), ( floor( $diff / 60 / 60 ) % 24 ) );

    } else{
        $text = date('j F Y', $date);

    }

    return $text;
}
/**
 * Time ago - Human time diff enhanced
 *
 * @param string    $timeformat  Fallback date format
 * @param int       $timestamp   Post or comment or anyhing's timestamp
 * @return string
 */
if( ! function_exists('cg_time_ago') ){
    function cg_time_ago( $timeformat = 'j F Y', $timestamp = '' ){
        if( ! $timestamp ){
            global $item;
            $timestamp = get_the_time( 'U', $item->ID );

            if( ! $timestamp ) return;
        }

        $diff = current_time('timestamp') - $timestamp;
        $is_today     = date( 'Y-m-d', current_time('timestamp') ) == date( 'Y-m-d', $timestamp );
        $is_yesterday = date( 'Y-m-d', current_time('timestamp') - 86400 ) == date( 'Y-m-d', $timestamp );

        if( $diff  < 0 ) return;

        // less than 2 minutes - print just now or moments ago
        if( $diff < 120 ){
            $text = __('Just now', 'coingeek');

        }
        // less than one hour - print x minutes ago
        elseif( $diff < 3600 ){
            $text = sprintf( __('%d minutes ago', 'coingeek'), ( floor( $diff / 60 ) % 60 ) );

        }
        // within an hour
        elseif( $diff < 7200 ) {
            $text = sprintf( __('%d hour ago', 'coingeek'), ( floor( $diff / 60 / 60 ) % 24 ) );

        }

        // belongs to today
        elseif( $is_today ){
            $text = sprintf( __('%d hours ago', 'coingeek'), ( floor( $diff / 60 / 60 ) % 24 ) );

        } else{
            $text = date_i18n( $timeformat, $timestamp );

        }

        return $text;
    }
}
add_filter( 'embed_oembed_html', 'wpse_embed_oembed_html', 99, 4 );
function wpse_embed_oembed_html( $cache, $url, $attr, $post_ID ) {
    $classes = array();

    // Add these classes to all embeds.
    $classes_all = array(
        'block-container',
    );

    // Check for different providers and add appropriate classes.

    if ( false !== strpos( $url, 'vimeo.com' ) ) {
        $classes[] = 'video-container vimeo';
    }

    if ( false !== strpos( $url, 'youtube.com' ) ) {
        $classes[] = 'video-container youtube';
    }

    if ( false !== strpos( $url, 'youtu.be' ) ) {
        $classes[] = 'video-container youtube';
    }

    $classes = array_merge( $classes, $classes_all );

    return '<div class="' . esc_attr( implode( $classes, ' ' ) ) . '">' . $cache . '</div>';
}