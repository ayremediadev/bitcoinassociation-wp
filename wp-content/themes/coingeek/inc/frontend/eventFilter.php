<?php
/**
 * Ajax filter Resource
 */
//get news by ajax
add_action( 'wp_ajax_filter_event', 'filter_event' );
add_action( 'wp_ajax_nopriv_filter_event', 'filter_event' );

function filter_event(){
    global $wp_query;
    $id = $_POST['post_id'];
    $banner_week = get_field('event_advertising_image', 'option');
    $banner_link = get_field('event_advertising_link', 'option');
    $month = !empty($_POST['month'])?$_POST['month']:null;
    $location = !empty($_POST['location'])?$_POST['location']:null;
    $type = !empty($_POST['type'])?$_POST['type']:null;
    $parameter_array = array('relation' => 'AND');
    $year = date("Y");
    $current_month = date('m');
    $day = date("d");
    if($current_month > $month){
        $year += 1;
    }
    if($month < 10){
        $month = "0".$month;
    }
    $query_nextMonth = $month;
    if($current_month == $month){
        $month .= date('d');
    }
    if(!empty($location)){
        $parameter_array[] = array(
            'taxonomy' => 'event_countries',
            'field'    => 'term_id',
            'terms'    => $location,
        );
    }
    if(!empty($type)){
        $parameter_array[] = array(
            'taxonomy' => 'category',
            'field'    => 'term_id',
            'terms'    => $type,
        );
    }
    $args = array(
        'post_type' => array(
            'event'
        ),
        'post_status' => array(
            'publish'
        ),
        'order' => 'ASC',
        'orderby' => 'from_date',
        'tax_query' => array(
            'relation' => 'AND'
        ),
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'from_date',
                'value' => $year.$month, // Lowest date value
                'compare' => '>='
            ),
            array(
                'key' => 'from_date',
                'value' => $year.$query_nextMonth."32", // Highest date value
                'compare' => '<'
            )
        ),
        'posts_per_page' => -1,
    );
    $args['tax_query'] = $parameter_array;
    $query = new WP_Query($args);
    ob_start();
    if ($query->have_posts()) {
        $post_idx = 0;
        $post_count = $query->post_count;
        while ($query->have_posts()) {
            $query->the_post(); $post_idx++;
            ?>
            <div class="col-3">
                <?php get_template_part('template_parts/metting__item'); ?>
            </div>
            <?php if($post_idx == 8 || ($post_idx < 8 && $post_idx == $post_count)): ?>
                <div class="col-12">
                    <div class="banner__section">
                        <?php if($banner_week): ?>
                            <div class="week__banner">
                                <a href="<?php echo $banner_link; ?>"><img src="<?php echo $banner_week['url'] ?>" alt=""></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php
        }
    }else{
        return false;
        wp_die();
    }
    $response = ob_get_contents();
    ob_end_clean();
    echo $response;
    wp_die();
}
?>