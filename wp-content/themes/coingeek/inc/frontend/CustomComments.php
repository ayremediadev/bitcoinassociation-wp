<?php
function athena_comment($comment, $args, $depth) {
    $date = human_time_diff( get_comment_date( 'U' ), current_time( 'timestamp' ) ).' '.__( 'ago' );
    $author_id=$comment->user_id;
    $avatar = get_field('image', 'user_'.$author_id);
    if(!empty($avatar)){
        $avatar = $avatar['sizes']['img_author'];
    }else{
        $avatar = get_avatar_url($author_id);
    }
    if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'div';
        $add_below = 'div-comment';
    }?>
    <<?php echo $tag; ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?> id="comment-<?php comment_ID() ?>"><?php
    if ( 'div' != $args['style'] ) { ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body"><?php
    } ?>
    <div class="comment-author vcard">
        <div class="author">
            <div class="author__image">
                <img src="<?php echo $avatar; ?>" alt="">
            </div>
            <div class="author__content">
                <p><?php printf( __( '<cite class="fn">%s</cite>' ), get_comment_author_link() ); ?></p>
                <p><?php comment_text(); ?></p>
            </div>
        </div>
        <div class="comment-meta commentmetadata date-comment">
            <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>"><?php
                /* translators: 1: date, 2: time */
                printf(
                    __('%1$s'),
                    $date
                ); ?>
            </a>
            <?php
            edit_comment_link( __( '(Edit)' ), '  ', '' );
            ?>
        </div>
    </div>
    <?php
    if ( $comment->comment_approved == '0' ) { ?>
        <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em><br/><?php
    } ?>

    <div class="reply"><?php
    comment_reply_link(
        array_merge(
            $args,
            array(
                'add_below' => $add_below,
                'depth'     => $depth,
                'max_depth' => $args['max_depth']
            )
        )
    ); ?>
    </div>
    <?php
    if ( 'div' != $args['style'] ) : ?>
        </div><?php
    endif;
    ?>
    </div>
<?php
}