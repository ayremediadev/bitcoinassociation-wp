<?php
//get news by ajax
add_action( 'wp_ajax_get_news_by_taxonomy', 'get_news_by_taxonomy' );
add_action( 'wp_ajax_nopriv_get_news_by_taxonomy', 'get_news_by_taxonomy' );

function get_news_by_taxonomy(){
    global $wp_query;
    $paged = !empty($_POST['page'])?$_POST['page']:0;
    $tax = !empty($_POST['tax'])?$_POST['tax']:"";
    $cat = !empty($_POST['cat'])?$_POST['cat']:"";
    $posttype = !empty($_POST['post_type'])?$_POST['post_type']:"post";
    $count_pages = !empty($_POST['max-page'])?$_POST['max-page']:0;
    $s = $_GET['s'];
    if($paged < $count_pages){
        $args = array(
            'post_type' => array(
                $posttype
            ),
            'post_status' => array(
                'publish'
            ),
            'order' => 'DESC',
            'orderby' => 'date',
            'tax_query' => $cat ? array(
                array(
                    'taxonomy' => $tax,
                    'field' => 'slug',
                    'terms' => $cat,
                ),
            ) : "",
            'posts_per_page' => 12,
            'paged' => $paged+1
        );
        if(!empty($s)){
            $args['s'] = $s;
        }
        $query = new WP_Query($args);
        ob_start();
        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                ?>
                <div class="col-3">
                    <?php get_template_part('template_parts/new'); ?>
                </div>
                <?php
            }
        }
        $response = ob_get_contents();
        ob_end_clean();
        echo $response;
    }else{
        return 0;
        wp_die();
    }
    wp_die();
}