 <?php
define('ASSETS_PATH', get_template_directory_uri().'/assets/');
define('DOMAIN', 'coingeek');
/**
 * Themes Option
**/
define('AT_LOGO', 'at_logo');
/**
 * Image Size
**/
global $img_new_size;
define('IMG_LATEST_NEWS', 'img_latest_news');
define('IMG_BUSINESS_THUMB', 'img_business_thumb');
define('IMG_NEW_NORMAL', 'img_new_normal');
define('IMG_NEW_SIDEBAR', 'img_new_sidebar');
define('IMG_NEW_BANNER', 'img_new_banner');
define('IMG_COINGEEK', 'img_coingeek');
define('IMG_COINGEEK_LARGE', 'img_coingeek_large');
define('IMG_SLIDER_HOME', 'img_slider_home');
define('IMG_EVENT_DETAIL', 'img_event_detail');
define('IMG_EXCHANGE_ITEM', 'img_exchange_item');
define('IMG_AUTHOR', 'img_author');
define('IMG_LOCATOR', 'img_locator');
define('IMG_VENTURE_DETAIL', 'image_venture_detail');
define('IMG_ICON_DONATION', 'image_icon_donation');
define('IMG_FUNDING_NORMAL', 'image_funding_normal');

