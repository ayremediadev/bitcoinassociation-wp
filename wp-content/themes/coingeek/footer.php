<?php
    $logo_img = get_field('footer_logo', 'option');
?>
<footer id="footer">
    <div class="wrapper">
        <div class="footer__container">
            <div class="footer__top">
                <div class="footer__logo">
                    <a href="<?php echo get_home_url(); ?>"><img src="<?php echo $logo_img; ?>" alt=""></a>
                </div>
                <div class="footer__menu left">
                    <?php
                    if ( has_nav_menu( 'athena_main_menu' ) ) {
                        wp_nav_menu( array(
                            'theme_location' => 'main_nav',
                            'menu' => 'footer-menu-left',
                            'menu_class' => '',
                            'container' => ''
                        ) );
                    }
                    ?>
                </div>
                <div class="footer__menu right">
                    <?php
                    if ( has_nav_menu( 'athena_main_menu' ) ) {
                        wp_nav_menu( array(
                            'theme_location' => 'main_nav',
                            'menu' => 'footer-menu-right',
                            'menu_class' => '',
                            'container' => ''
                        ) );
                    }
                    ?>
                </div>
            </div>
            <div class="footer__bottom">
                <div class="footer__partner">
                    <ul>
                        <?php
                            if( have_rows('footer_image', 'option') ):
                                while( have_rows('footer_image', 'option') ): the_row();
                                    $image = get_sub_field('image');
                                    $link = get_sub_field('link');
                        ?>
                                    <li><a href="<?php echo $link; ?>" target="_blank"><img src="<?php echo $image['url']; ?>" alt=""></a></li>
                        <?php
                                endwhile;
                            endif;
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="share__button--mobile">
    <div class="content">
        <div class="onesignal-bell-launcher-message">
            <div class="share">Share</div>
        </div>
        <svg fill="#fff" preserveAspectRatio="xMidYMid meet" height="2em" width="2em" viewBox="0 0 40 40">
            <g>
                <path d="m30 26.8c2.7 0 4.8 2.2 4.8 4.8s-2.1 5-4.8 5-4.8-2.3-4.8-5c0-0.3 0-0.7 0-1.1l-11.8-6.8c-0.9 0.8-2.1 1.3-3.4 1.3-2.7 0-5-2.3-5-5s2.3-5 5-5c1.3 0 2.5 0.5 3.4 1.3l11.8-6.8c-0.1-0.4-0.2-0.8-0.2-1.1 0-2.8 2.3-5 5-5s5 2.2 5 5-2.3 5-5 5c-1.3 0-2.5-0.6-3.4-1.4l-11.8 6.8c0.1 0.4 0.2 0.8 0.2 1.2s-0.1 0.8-0.2 1.2l11.9 6.8c0.9-0.7 2.1-1.2 3.3-1.2z"></path>
            </g>
        </svg>
    </div>
</div>
<?php wp_footer(); ?>
<script id="dsq-count-scr" src="//coingeek.disqus.com/count.js" async></script>
</body>
</html>
