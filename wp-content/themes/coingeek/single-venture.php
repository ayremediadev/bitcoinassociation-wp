<?php get_header('funding'); ?>
<?php
    $hero_tite = get_field('hero_title')?get_field('hero_title'):"<strong>".get_the_title()."</strong>";
    $tertiary_title = get_field('tertiary_title');
    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), IMG_VENTURE_DETAIL );
?>
<main id="main-content">
    <div class="title__non-results news__funding">
        <div class="wrapper">
            <div class="row">
                <div class="col-8">
                    <p><?= $hero_tite; ?></p>
                    <span><?= $tertiary_title ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="include__funding--content">
        <div class="wrapper">
            <div class="row">
                <div class="col-10">
                    <div class="page__content--main">
                        <div class="box__content <?php if(!empty($image_url)) echo "have__image"; ?>">
                            <?php if(!empty($image_url)): ?>
                                <div class="img">
                                    <img src="<?php echo $image_url[0] ?>" alt="<?php the_title(); ?>">
                                </div>
                            <?php endif; ?>
                            <div class="content__text">
                                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                    <?php the_content(); ?>
                                <?php endwhile; endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-2">
                    <?php get_template_part('template_parts/sidebar__page'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php get_template_part('template_parts/newsletters');  ?>
</main>
<?php get_footer(); ?>
