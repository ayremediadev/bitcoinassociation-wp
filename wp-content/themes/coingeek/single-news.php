<?php
    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), IMG_NEW_BANNER);
    $terms = get_the_terms($post->ID, 'category_new');
    $template = 1;
    $author = get_the_author($post->ID);
    $date = human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ).' '.__( 'ago' );
    $author_id=$post->post_author;
    $author_link = get_author_posts_url($author_id);
    $avatar = get_field('image', 'user_'.$author_id);
    if(!empty($avatar)){
        $avatar = $avatar['sizes']['img_author'];
    }else{
        $avatar = get_avatar_url($author_id);
    }
    $post_note = get_field('note_post', 'option');
?>
<?php get_header(); ?>
<main id="main-content" class="<?php if($template) echo "has-sidebar" ?>">
    <div class="new__banner">
        <img src="<?php echo $image_url[0]; ?>" alt="">
    </div>
    <div class="wrapper">
        <?php if($template): ?>
        <div class="row">
        <?php endif; ?>
            <div class="new__wrapper <?php if($template) echo "col-8" ?>">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="new__container">
                    <div class="new__detail--top">
                        <p class="new__info">
                            <span class="new__info--title"><?php echo $terms[0]->name;; ?></span> <span class="new__info--date"> <?php echo cg_time_ago(); ?></span>
                        </p>
                        <a href="<?php echo $author_link; ?>" class="author">
                            <span><?php echo get_the_author_link($author_id); ?> </span>
                            <img src="<?= $avatar; ?>" alt="">
                        </a>
                    </div>
                    <h2 class="title"><?php the_title(); ?></h2>
                    <div class="content">
                        <?php the_content(); ?>
                        <?php echo $post_note; ?>
                    </div>
                </div>
                <?php get_template_part('template_parts/new__tags'); ?>
                <?php get_template_part('template_parts/new__comment'); ?>
            <?php endwhile; endif; ?>
            </div>
            <div class="home__section_content <?php if($template) echo "col-4" ?>">
                <?php get_template_part('template_parts/related_news'); ?>
            </div>
        <?php if($template): ?>
        </div>
        <?php endif; ?>
    </div>
    <?php get_template_part('template_parts/newsletters');  ?>
</main>
<?php get_footer(); ?>
