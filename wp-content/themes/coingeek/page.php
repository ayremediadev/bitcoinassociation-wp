<?php get_header(); ?>
<?php
    $hero_tite = get_field('hero_title')?get_field('hero_title'):"<strong>".get_the_title()."</strong>";
?>
<main id="main-content">
    <div class="title__non-results">
        <div class="wrapper">
            <div class="row">
                <div class="col-8">
                    <p><?= $hero_tite; ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="page__content">
        <div class="wrapper">
            <div class="row">
                <div class="col-10">
                    <div class="page__content--main">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; endif; ?>
                    </div>
                </div>
                <?php
                if(have_rows('sidebar_pages')):
                ?>
                <div class="col-2">
                    <?php get_template_part('template_parts/sidebar__page'); ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php get_template_part('template_parts/news__latest__page');  ?>
    <?php get_template_part('template_parts/newsletters');  ?>
</main>
<?php get_footer(); ?>
