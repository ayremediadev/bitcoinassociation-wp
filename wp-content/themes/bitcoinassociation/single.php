<?php
/**
 * The template for displaying all single posts.
 *
 * @package Total
 */

get_header(); ?>

<header class="ht-main-header">
	<div class="ht-container">
		<div class="ht-feat-img" style="text-align: center;">
			<?php the_post_thumbnail(); ?>
		</div>
		<?php the_title( '<h1 style="margin-top: 1em;" class="ht-main-title">', '</h1>' ); 
		$show_author = get_field('display_author'); 
		$author = get_the_author(); 
		if($show_author == 'Yes'){
			echo '<h4>By '. $author .'</h4>'; 
		}
		do_action( 'total_breadcrumbs' ); ?>
	</div>
</header><!-- .entry-header -->

<div class="ht-container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'single' ); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>

		<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>

</div>

<?php get_footer(); 