<?php
	
//wp_enqueue_style( 'bacss', get_template_directory_uri() . '/style.css', array(), time() );
	
function wdo_disable_srcset( $sources ) {
    return false;
}
add_filter( 'wp_calculate_image_srcset', 'wdo_disable_srcset' );

add_action( 'wp_enqueue_scripts', 'enqueue_child_theme_styles', PHP_INT_MAX);
function enqueue_child_theme_styles() {
  wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 * Register support for Gutenberg wide images in your theme
 */
function mytheme_setup() {
  add_theme_support( 'align-wide' );
}
add_action( 'after_setup_theme', 'mytheme_setup' );